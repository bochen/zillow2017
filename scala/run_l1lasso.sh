TRAIN=../input/train2016_cat.csv
TARGET=target/scala-2.11/z2017-assembly-1.0.jar

MAIN=zillow.SparkL1LASSOReg
WAIT=1
ENABLE_LOG=false
I=/tmp/all_field_count.txt.reduced.sorted2.libsvm3
 
nohup $HOME/spark/bin/spark-submit --master spark://genomics-ecs1:7077 --deploy-mode client --driver-memory 55G --driver-cores 5 --executor-memory 18G  --executor-cores 2 --conf spark.default.parallelism=8100 --conf spark.driver.maxResultSize=20g --conf spark.network.timeout=360000 --conf spark.eventLog.enabled=$ENABLE_LOG --conf spark.speculation=true --class $MAIN $TARGET \
-i $I -n 10 -r 0.005 -e 0.8 &
#-i /tmp/lass_feats_30_0.003_1.0-lass_feats_20_0.003_0.8.txt.libsvm -n 100 -r 0.003 -e 0.8 --sqrt_y &

