package zillow

import com.holdenkarau.spark.testing.DataFrameSuiteBase
import org.scalatest.{BeforeAndAfter, FlatSpec, Matchers}

/**
  * Created by Lizhen Shi on 6/11/17.
  */
class SparkL1LASSORegSpec extends FlatSpec with Matchers with BeforeAndAfter with DataFrameSuiteBase {

  val name = Utils.home + "/mydev/zillow2017/scala/field_count_1.txt.libsvm2"
  //var name = Utils.home + "/spark/data/mllib/sample_linear_regression_data.txt"
  "SparkL1LASSOReg" should "work  " in {
    val args =  s"-i $name -n 10 -r ${1.0/500} -e 1.0".split(" ").filter(_.nonEmpty)

    SparkL1LASSOReg.main(args, spark)
  }


}
