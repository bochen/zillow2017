package zillow

import com.holdenkarau.spark.testing.DataFrameSuiteBase
import com.typesafe.scalalogging.LazyLogging
import org.apache.spark.ml.regression.LinearRegression
import org.apache.spark.ml.regression.L1Regression
import org.scalatest.{BeforeAndAfter, FlatSpec, Matchers}


/**
  * Created by bo on 8/6/17.
  */

class L1RegressionTest extends FlatSpec with Matchers with BeforeAndAfter with DataFrameSuiteBase with LazyLogging {

  val name = Utils.home + "/spark/data/mllib/sample_linear_regression_data.txt"

  def logInfo(s:String)={
    println("AAAA: "+s)
    logger.info(s)
  }

  "Spark LinearRegression" should "work  " in {
    val training = spark.read.format("libsvm").load(name)

    val lr = new LinearRegression()
      .setMaxIter(10)
      .setRegParam(0.3)
      .setElasticNetParam(0.8)

    // Fit the model
    val lrModel = lr.fit(training)

    // Print the coefficients and intercept for linear regression
    logInfo(s"Coefficients size: ${lrModel.coefficients.size} Intercept: ${lrModel.intercept}")
    logInfo(s"#None zero coefficients: ${lrModel.coefficients.numNonzeros}")
    // Summarize the model over the training set and print out some metrics
    val trainingSummary = lrModel.summary
    logInfo(s"objectiveHistory: [${trainingSummary.objectiveHistory.mkString(",")}]")
    trainingSummary.residuals.show()
    logInfo(s"numIterations: ${trainingSummary.totalIterations}")
    logInfo(s"RMSE: ${trainingSummary.rootMeanSquaredError}")
    logInfo(s"MAE: ${trainingSummary.meanAbsoluteError}")
    logInfo(s"r2: ${trainingSummary.r2}")

  }

  "spark L1Regression" should "work" in {
    val training = spark.read.format("libsvm").load(name)

    val lr = new L1Regression()
      .setMaxIter(10)
      .setRegParam(0.01)
      .setElasticNetParam(0.8)

    // Fit the model
    val lrModel = lr.fit(training)

    // Print the coefficients and intercept for linear regression
    logInfo(s"Coefficients size: ${lrModel.coefficients.size} Intercept: ${lrModel.intercept}")
    logInfo(s"#None zero coefficients: ${lrModel.coefficients.numNonzeros}")
    // Summarize the model over the training set and print out some metrics
    val trainingSummary = lrModel.summary
    logInfo(s"objectiveHistory: [${trainingSummary.objectiveHistory.mkString(",")}]")
    trainingSummary.residuals.show()
    logInfo(s"numIterations: ${trainingSummary.totalIterations}")
    logInfo(s"RMSE: ${trainingSummary.rootMeanSquaredError}")
    logInfo(s"MAE: ${trainingSummary.meanAbsoluteError}")
    logInfo(s"r2: ${trainingSummary.r2}")
  }


}