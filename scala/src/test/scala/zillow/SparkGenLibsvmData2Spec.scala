package zillow

import com.holdenkarau.spark.testing.SharedSparkContext
import org.scalatest.{BeforeAndAfter, FlatSpec, Matchers}

/**
  * Created by Lizhen Shi on 6/11/17.
  */
class SparkGenLibsvmData2Spec extends FlatSpec with Matchers with BeforeAndAfter with SharedSparkContext {

  val name = Utils.home + "/mydev/zillow2017/input/train2016_cat.csv.small"
  "SparkGenLibsvmData2" should "work  on n=1" in {
    val args = Array(name, "field_count_1.txt")

    SparkGenLibsvmData2.main(args, sc)
  }

  "SparkGenLibsvmData2" should "work  on n=2" in {
    val args = Array(name, "field_count_2.txt")

    SparkGenLibsvmData2.main(args, sc)
  }


}
