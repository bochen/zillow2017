package zillow

import com.holdenkarau.spark.testing.{DataFrameSuiteBase, SharedSparkContext}
import org.scalatest.{BeforeAndAfter, FlatSpec, Matchers}
import zillow.Utils

/**
  * Created by Lizhen Shi on 6/11/17.
  */
class SparkLASSORegSpec extends FlatSpec with Matchers with BeforeAndAfter with DataFrameSuiteBase {

  val name = Utils.home + "/mydev/zillow2017/scala/field_count_1.txt.libsvm2"
  //var name = Utils.home + "/spark/data/mllib/sample_linear_regression_data.txt"
  "SparkLASSOReg" should "work  " in {
    val args =  s"-i $name -n 10 -r ${1.0/300} -e 1.0".split(" ").filter(_.nonEmpty)

    SparkLASSOReg.main(args, spark)
  }

  "SparkLASSOReg" should "work  with sqrt y" in {
    val args =  s"-i $name -n 10 -r ${1.0/300} -e 1.0 --sqrt_y".split(" ").filter(_.nonEmpty)

    SparkLASSOReg.main(args, spark)
  }


}
