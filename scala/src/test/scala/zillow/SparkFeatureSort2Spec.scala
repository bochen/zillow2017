package zillow

import com.holdenkarau.spark.testing.SharedSparkContext
import org.scalatest.{BeforeAndAfter, FlatSpec, Matchers}
import zillow.Utils

/**
  * Created by Lizhen Shi on 6/11/17.
  */
class SparkFeatureSort2Spec extends FlatSpec with Matchers with BeforeAndAfter with SharedSparkContext {

  val name = Utils.home + "/mydev/zillow2017/input/train2016_cat.csv.small"
  "SparkFeatureSort2" should "work  on n=1" in {
    val args = Array(name,"field_count_1.txt")

    SparkFeatureSort2.main(args, sc)
  }

   "SparkFeatureSort2" should "work  on n=2" in {
    val args = Array(name,"field_count_2.txt")

    SparkFeatureSort2.main(args, sc)
  }



}
