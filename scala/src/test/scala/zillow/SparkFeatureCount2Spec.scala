package zillow

import com.holdenkarau.spark.testing.SharedSparkContext
import org.scalatest.{BeforeAndAfter, FlatSpec, Matchers}

/**
  * Created by Lizhen Shi on 6/11/17.
  */
class SparkFeatureCount2Spec extends FlatSpec with Matchers with BeforeAndAfter with SharedSparkContext {

  val name = Utils.home + "/mydev/zillow2017/input/train2016_cat.csv.small"
  "SparkFeatureCount2" should "work  on n=1" in {
    val args = s"-i $name -n 1 --min_count 40 -k 200".split(" ")
      .filter(_.nonEmpty)

    SparkFeatureCount2.main(args, sc)
  }

  "SparkFeatureCount2" should "work  on n=2" in {
    val args = s"-i $name -n 2 --min_count 40 -k 200 -s 0 ".split(" ")
      .filter(_.nonEmpty)

    SparkFeatureCount2.main(args, sc)
  }

  "SparkFeatureCount2" should "work  on n=2 and s=1" in {
    val args = s"-i $name -n 2 --min_count 40 -k 200 -s 1 -g 0.4 ".split(" ")
      .filter(_.nonEmpty)

    SparkFeatureCount2.main(args, sc)
  }


}
