package zillow

import com.typesafe.scalalogging.LazyLogging
import org.apache.spark.broadcast.Broadcast
import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}
import sext._

/**
  * Created by Lizhen Shi on 6/11/17.
  */


object SparkFeatureCount extends LazyLogging {

  case class Config(fname: String = "", n: Int = 1, min_count: Int = 10, sleep: Int = 0, s: Int = 0, k: Int = 1000000)

  def parse_command_line(args: Array[String]): Option[Config] = {
    val parser = new scopt.OptionParser[Config]("FeatureCount") {

      opt[String]('i', "input").required().valueName("<file>").action((x, c) =>
        c.copy(fname = x)).text("input train data file")

      opt[Int]('n', "numcol").required().action((x, c) =>
        c.copy(n = x))
        .text("number of columns in a field")

      opt[Int]('k', "k").required().action((x, c) =>
        c.copy(k = x))
        .text("desired count")
      opt[Int]('s', "s").action((x, c) =>
        c.copy(s = x))
        .text("sample method")

      opt[Int]('m', "min_count").action((x, c) =>
        c.copy(min_count = x))
        .text("minimum count to preserve")

      opt[Int]("wait").action((x, c) =>
        c.copy(sleep = x))
        .text("wait $slep second before stop spark session. For debug purpose, default 0.")

    }

    parser.parse(args, Config())
  }


  def sample0(unqiue_fields: RDD[(Field, Int)], desired_count: Int, data: Broadcast[Array[Record]]): Array[(Field, Int)] = {
    unqiue_fields.takeSample(withReplacement = false, num = desired_count)
  }

  def sample1(unqiue_fields: RDD[(Field, Int)], desired_count: Int, data: Broadcast[Array[Record]]): Array[(Field, Int)] = {
    import org.apache.spark.mllib.clustering.BisectingKMeans
    import org.apache.spark.mllib.linalg.Vectors
    val rdd = unqiue_fields.map {
      case (field, cnt) =>
        Vectors.dense(field.value(data.value).map(_.toDouble))
    }.cache()
    val bkm = new BisectingKMeans().setK(desired_count)
    val model = bkm.run(rdd)

    // Show the compute cost and the cluster centers
    logger.info(s"Compute Cost: ${model.computeCost(rdd)}")

    val centers = model.predict(rdd)
    val samples = centers.zip(unqiue_fields).reduceByKey((u, v) => u).map(_._2).collect()
    rdd.unpersist()
    samples
  }


  def run(X: Array[Array[Int]], prev_fields: Array[Field], config: Config, sc: SparkContext): Array[(Field, Int)] = {
    val max_n = X(0).length
    val data = sc.broadcast(X.map(Record(_)))
    val expand_cols = {
      val tmp = X.map(u => u.map(u => if (u > 0) 1 else 0))
      (0 until X(0).length).map(j => (j, 0.until(X.length).map(i => tmp(i)(j)).sum)).filter(_._2 >= config.min_count).map(_._1).toArray
    }
    val fields = sc.parallelize(prev_fields).map(_.expand(expand_cols)).flatMap(x => x).distinct()
    val candidate_cnt = fields.count
    logger.info(s"candidate fields number=${candidate_cnt}")
    val unqiue_fields = fields.map {
      f =>
        (f, data.value.map(f.value(_)).sum)
    }.filter(_._2 >= config.min_count)

    val r = if (config.s == 0) sample0(unqiue_fields, config.k, data) else sample1(unqiue_fields, config.k, data)
    data.destroy()
    logger.info(s"filter ${r.length} fields out of ${candidate_cnt}")
    r
  }

  def main(args: Array[String]): Unit = {
    val conf = new SparkConf().setAppName("FeatureCount")
    val sc = new SparkContext(conf)
    main(args, sc)
  }

  def main(args: Array[String], sc: SparkContext): Unit = {
    val options = parse_command_line(args)

    options match {
      case Some(_) =>
        val config = options.get

        logger.info(s"called with arguments\n${options.valueTreeString}")

        val fname = config.fname
        val n = config.n
        val fout = s"field_count_$n.txt"

        val prev_fields =
          if (n == 1)
            Array(Field.empty())
          else {
            val fin = s"field_count_${n - 1}.txt"
            Utils.read_text(fin).map(_.split("\t")).filter(x => x(1).toInt >= config.min_count).map(_ (0)).map(Field(_))
          }

        logger.info(s"Run with data=${fname}, n=$n, prev_len=${prev_fields.length}")

        val (trainX, trainy) = Utils.read_csv(fname, has_y = true)
        require(trainX(0).length == 500)
        require(trainy.length == trainX.length)
        val field_counts = run(trainX, prev_fields, config, sc)
        Utils.write_text(field_counts.map(u => u._1.toString + "\t" + u._2.toString), fout)
        logger.info(s"write ${field_counts.length} fields to $fout")

        if (config.sleep > 0) Thread.sleep(config.sleep * 1000)
        sc.stop()
      case None =>
        println("bad arguments")
        sys.exit(-1)
    }


  }
}
