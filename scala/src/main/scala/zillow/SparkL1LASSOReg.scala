package zillow

import com.typesafe.scalalogging.LazyLogging
import org.apache.spark.ml.regression.{L1Regression, LinearRegression}
import org.apache.spark.sql.SparkSession
import sext._

/**
  * Created by Lizhen Shi on 6/11/17.
  */


object SparkL1LASSOReg extends LazyLogging {

  case class Config(fname: String = "", n: Int = 1, reg: Double = 0.3, elasnet: Double = 1, sleep: Int = 0, y_scale: Double = 15.0)

  def parse_command_line(args: Array[String]): Option[Config] = {
    val parser = new scopt.OptionParser[Config]("FeatureCount") {

      opt[String]('i', "input").required().valueName("<file>").action((x, c) =>
        c.copy(fname = x)).text("input libsvm file")

      opt[Int]('n', "n_interation").required().action((x, c) =>
        c.copy(n = x))
        .text("number of interation")

      opt[Double]('r', "reg").required().action((x, c) =>
        c.copy(reg = x))
        .text("regularization")
      opt[Double]('e', "elasnet").required().action((x, c) =>
        c.copy(elasnet = x))
        .text("elasnet parameter")

      opt[Double]("y_scale").action((x, c) =>
        c.copy(y_scale = x))
        .text("scaling of y")

      opt[Int]("wait").action((x, c) =>
        c.copy(sleep = x))
        .text("wait $slep second before stop spark session. For debug purpose, default 0.")

    }

    parser.parse(args, Config())
  }

  def main(args: Array[String]): Unit = {
    val spark = SparkSession
      .builder
      .appName("SparkLASSOReg")
      .getOrCreate()

    main(args, spark)
    spark.stop()
  }


  def main(args: Array[String], spark: SparkSession): Unit = {

    val options = parse_command_line(args)

    options match {
      case Some(_) =>
        val config = options.get

        logger.info(s"called with arguments\n${options.valueTreeString}")


        // Load training data
        val raw_training = spark.read.format("libsvm").load(config.fname)
        val training = if (true) {
          def scaling_y(u: Double) = {
            u*config.y_scale
          }
          import org.apache.spark.sql.functions.{col, udf}
          val scaling_y_udf = udf(scaling_y _)

          raw_training.withColumn("label", scaling_y_udf(col("label")))
        } else
          raw_training
        val lr = new L1Regression()
          .setMaxIter(config.n)
          .setRegParam(config.reg)
          .setElasticNetParam(config.elasnet)

        // Fit the model
        val lrModel = lr.fit(training)

        // Print the coefficients and intercept for linear regression
        logger.info(s"Coefficients size: ${lrModel.coefficients.size} Intercept: ${lrModel.intercept}")
        logger.info(s"#None zero coefficients: ${lrModel.coefficients.numNonzeros}")
        // Summarize the model over the training set and print out some metrics
        val trainingSummary = lrModel.summary
        logger.info(s"objectiveHistory: [${trainingSummary.objectiveHistory.mkString(",")}]")
        trainingSummary.residuals.show()
        logger.info(s"numIterations: ${trainingSummary.totalIterations}")
        logger.info(s"RMSE: ${trainingSummary.rootMeanSquaredError}")
        logger.info(s"MAE: ${trainingSummary.meanAbsoluteError}")
        logger.info(s"r2: ${trainingSummary.r2}")

        val lst = collection.mutable.ArrayBuffer.empty[String]
        lrModel.coefficients.foreachActive {
          case (i, d) =>
            lst.append(f"${i}\t$d%.5f")
        }
        //lst.foreach(println)
        Utils.write_text(lst.toArray, s"l1lasso_feats_${config.n}_${config.reg}_${config.elasnet}.txt")


        if (config.sleep > 0) Thread.sleep(config.sleep * 1000)
      case None =>
        println("bad arguments")
        sys.exit(-1)
    }

  }


}