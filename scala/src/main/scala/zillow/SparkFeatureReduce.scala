package zillow

import com.typesafe.scalalogging.LazyLogging
import org.apache.spark.{SparkConf, SparkContext}

/**
  * Created by Lizhen Shi on 6/11/17.
  */


object SparkFeatureReduce extends LazyLogging {


  def main(args: Array[String]): Unit = {
    val conf = new SparkConf().setAppName("FeatureReduce")
    val sc = new SparkContext(conf)
    main(args, sc)
  }

  def main(args: Array[String], sc: SparkContext): Unit = {

    if (args.length != 4) {
      println("cmd datafile,featsfile,count,s")
      return
    }

    val fin = args(1)
    val desired_count = args(2).toInt
    require(desired_count > 0)
    val s = args(3).toInt
    val (trainX, trainy) = Utils.read_csv(args(0), has_y = true)
    val feats = sc.parallelize(Utils.read_text(fin)).map(_.split("\t")).map(u => (Field(u(0)), u(1).toInt))
    val X = sc.broadcast(trainX.map(u => u.map(_ > 0)).map(Record(_)))
    val unqiues = feats.map {
      a =>
        val feat = a._1
        (feat.value(X.value).map(u => u > 0).toVector, a)
    }.reduceByKey((u, v) => u).map(_._2)
    val filtered =
      (if (s == 0)
        SparkFeatureCount.sample0(unqiues, desired_count, X)
      else if(s==1)
        SparkFeatureCount.sample1(unqiues, desired_count, X)
      else
        unqiues.collect()
        ).map(u => u._1.toString + "\t" + u._2.toString)

    Utils.write_text(filtered, fin + ".reduced")
    X.destroy()
    println("end")
    sc.stop()

  }


}
