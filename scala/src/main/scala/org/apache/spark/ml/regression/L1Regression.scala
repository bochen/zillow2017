package org.apache.spark.ml.regression

import breeze.linalg.{DenseVector => BDV}
import breeze.optimize.{CachedDiffFunction, DiffFunction, LBFGS => BreezeLBFGS, OWLQN => BreezeOWLQN}
import breeze.stats.distributions.StudentsT
import org.apache.hadoop.fs.Path
import org.apache.spark.SparkException
import org.apache.spark.annotation.{Experimental, Since}
import org.apache.spark.internal.Logging
import org.apache.spark.ml.PredictorParams
import org.apache.spark.ml.feature.Instance
import org.apache.spark.ml.linalg.BLAS._
import org.apache.spark.ml.linalg.{Vector, Vectors}
import org.apache.spark.ml.optim.WeightedLeastSquares
import org.apache.spark.ml.param.ParamMap
import org.apache.spark.ml.param.shared._
import org.apache.spark.ml.util._
import org.apache.spark.mllib.evaluation.RegressionMetrics
import org.apache.spark.mllib.linalg.VectorImplicits._
import org.apache.spark.mllib.linalg.{Vectors => OldVectors}
import org.apache.spark.mllib.stat.MultivariateOnlineSummarizer
import org.apache.spark.mllib.util.MLUtils
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.functions._
import org.apache.spark.sql.types.DoubleType
import org.apache.spark.sql.{DataFrame, Dataset, Row}
import org.apache.spark.storage.StorageLevel

import scala.collection.mutable

/**
  * Params for linear regression.
  */
private[regression] trait L1RegressionParams extends PredictorParams
  with HasRegParam with HasElasticNetParam with HasMaxIter with HasTol
  with HasFitIntercept with HasStandardization with HasWeightCol with HasSolver

/**
  * Linear regression.
  *
  * The learning objective is to minimize the squared error, with regularization.
  * The specific squared error loss function used is:
  * L = 1/2n ||A coefficients - y||^2^
  *
  * This supports multiple types of regularization:
  *  - none (a.k.a. ordinary least squares)
  *  - L2 (ridge regression)
  *  - L1 (Lasso)
  *  - L2 + L1 (elastic net)
  */
@Since("1.3.0")
class L1Regression @Since("1.3.0")(@Since("1.3.0") override val uid: String)
  extends Regressor[Vector, L1Regression, L1RegressionModel]
    with L1RegressionParams with DefaultParamsWritable with Logging {

  @Since("1.4.0")
  def this() = this(Identifiable.randomUID("linReg"))

  /**
    * Set the regularization parameter.
    * Default is 0.0.
    *
    * @group setParam
    */
  @Since("1.3.0")
  def setRegParam(value: Double): this.type = set(regParam, value)

  setDefault(regParam -> 0.0)


  /**
    * Set the ElasticNet mixing parameter.
    * For alpha = 0, the penalty is an L2 penalty. For alpha = 1, it is an L1 penalty.
    * For 0 < alpha < 1, the penalty is a combination of L1 and L2.
    * Default is 0.0 which is an L2 penalty.
    *
    * @group setParam
    */
  @Since("1.4.0")
  def setElasticNetParam(value: Double): this.type = set(elasticNetParam, value)

  setDefault(elasticNetParam -> 0.0)

  /**
    * Set the maximum number of iterations.
    * Default is 100.
    *
    * @group setParam
    */
  @Since("1.3.0")
  def setMaxIter(value: Int): this.type = set(maxIter, value)

  setDefault(maxIter -> 100)

  /**
    * Set the convergence tolerance of iterations.
    * Smaller value will lead to higher accuracy with the cost of more iterations.
    * Default is 1E-6.
    *
    * @group setParam
    */
  @Since("1.4.0")
  def setTol(value: Double): this.type = set(tol, value)

  setDefault(tol -> 1E-6)

  /**
    * Whether to over-/under-sample training instances according to the given weights in weightCol.
    * If not set or empty, all instances are treated equally (weight 1.0).
    * Default is not set, so all instances have weight one.
    *
    * @group setParam
    */
  @Since("1.6.0")
  def setWeightCol(value: String): this.type = set(weightCol, value)


  /**
    * Gets the nth percentile entry for an RDD of doubles
    *
    * @param inputScore : Input scores consisting of a RDD of doubles
    * @param percentile : The percentile cutoff required (between 0 to 100), e.g 90%ile of [1,4,5,9,19,23,44] = ~23.
    *                   It prefers the higher value when the desired quantile lies between two data points
    * @return : The number best representing the percentile in the Rdd of double
    */
  def getRddPercentile(inputScore: RDD[Double], percentile: Double): Double = {
    inputScore.cache()
    val numEntries = inputScore.count().toDouble
    val retrievedEntry = (percentile * numEntries).min(numEntries).max(0).toInt


    val ret = inputScore
      .sortBy { case (score) => score }
      .zipWithIndex()
      .filter { case (score, index) => index == retrievedEntry }
      .map { case (score, index) => score }
      .collect()(0)
    inputScore.unpersist()
    ret
  }


  override protected def train(dataset: Dataset[_]): L1RegressionModel = {
    // Extract the number of features before deciding optimization solver.
    val numFeatures = dataset.select(col($(featuresCol))).first().getAs[Vector](0).size
    val w = if (!isDefined(weightCol) || $(weightCol).isEmpty) lit(1.0) else col($(weightCol))

    if (($(solver) == "auto" && $(elasticNetParam) == 0.0 &&
      numFeatures <= WeightedLeastSquares.MAX_NUM_FEATURES) || $(solver) == "normal") {
      throw new Exception("Not supported")
    }

    val instances: RDD[Instance] =
      dataset.select(col($(labelCol)), w, col($(featuresCol))).rdd.map {
        case Row(label: Double, weight: Double, features: Vector) =>
          Instance(label, weight, features)
      }

    val handlePersistence = dataset.rdd.getStorageLevel == StorageLevel.NONE
    if (handlePersistence) instances.persist(StorageLevel.MEMORY_AND_DISK)

    val (featuresSummarizer, ySummarizer) = {
      val seqOp = (c: (MultivariateOnlineSummarizer, MultivariateOnlineSummarizer),
                   instance: Instance) =>
        (c._1.add(instance.features, instance.weight),
          c._2.add(Vectors.dense(instance.label), instance.weight))

      val combOp = (c1: (MultivariateOnlineSummarizer, MultivariateOnlineSummarizer),
                    c2: (MultivariateOnlineSummarizer, MultivariateOnlineSummarizer)) =>
        (c1._1.merge(c2._1), c1._2.merge(c2._2))

      instances.treeAggregate(
        new MultivariateOnlineSummarizer, new MultivariateOnlineSummarizer)(seqOp, combOp)
    }
    var yMedian = getRddPercentile(instances.map(u => u.label), 0.5)

    val yMean = ySummarizer.mean(0)
    val rawYStd = math.sqrt(ySummarizer.variance(0))
    if (rawYStd == 0.0) {
      throw new Exception("y std is zero")
    }

    // if y is constant (rawYStd is zero), then y cannot be scaled. In this case
    // setting yStd=abs(yMean) ensures that y is not scaled anymore in l-bfgs algorithm.
    val yStd = if (rawYStd > 0) rawYStd else math.abs(yMean)
    val featuresMean = featuresSummarizer.mean.toArray
    val featuresStd = featuresSummarizer.variance.toArray.map(math.sqrt)

    if (!$(fitIntercept) && (0 until numFeatures).exists { i =>
      featuresStd(i) == 0.0 && featuresMean(i) != 0.0
    }) {
      logWarning("Fitting L1RegressionModel without intercept on dataset with " +
        "constant nonzero column, Spark MLlib outputs zero coefficients for constant nonzero " +
        "columns. This behavior is the same as R glmnet but different from LIBSVM.")
    }

    if (!$(fitIntercept)) throw new Exception("fitIntercept=false not supported.")
    val effectiveRegParam = $(regParam)
    val effectiveL1RegParam = $(elasticNetParam) * effectiveRegParam
    val effectiveL2RegParam = (1.0 - $(elasticNetParam)) * effectiveRegParam

    val costFun = new L1CostFun(instances, yStd, yMedian, effectiveL2RegParam)

    val optimizer = if ($(elasticNetParam) == 0.0 || effectiveRegParam == 0.0) {
      new BreezeLBFGS[BDV[Double]]($(maxIter), 10, $(tol))
    } else {
      val standardizationParam = $(standardization)

      def effectiveL1RegFun = (index: Int) => {
        if (standardizationParam) {
          effectiveL1RegParam
        } else {
          // If `standardization` is false, we still standardize the data
          // to improve the rate of convergence; as a result, we have to
          // perform this reverse standardization by penalizing each component
          // differently to get effectively the same objective function when
          // the training dataset is not standardized.
          if (featuresStd(index) != 0.0) effectiveL1RegParam / featuresStd(index) else 0.0
        }
      }

      new BreezeOWLQN[Int, BDV[Double]]($(maxIter), 10, effectiveL1RegFun, $(tol))
    }

    val initialCoefficients = Vectors.zeros(numFeatures)
    val states = optimizer.iterations(new CachedDiffFunction(costFun),
      initialCoefficients.asBreeze.toDenseVector)

    val (coefficients, objectiveHistory) = {
      /*
         Note that in Linear Regression, the objective history (loss + regularization) returned
         from optimizer is computed in the scaled space given by the following formula.
         {{{
         L = 1/2n||\sum_i w_i(x_i - \bar{x_i}) / \hat{x_i} - (y - \bar{y}) / \hat{y}||^2 + regTerms
         }}}
       */
      val arrayBuilder = mutable.ArrayBuilder.make[Double]
      var state: optimizer.State = null
      while (states.hasNext) {
        state = states.next()
        arrayBuilder += state.adjustedValue
      }
      if (state == null) {
        val msg = s"${optimizer.getClass.getName} failed."
        logError(msg)
        throw new SparkException(msg)
      }

      if (!state.actuallyConverged) {
        logWarning("L1Regression training finished but the result " +
          s"is not converged because: ${state.convergedReason.get.reason}")
      }

      /*
         The coefficients are trained in the scaled space; we're converting them back to
         the original space.
       */
      val rawCoefficients = state.x.toArray.clone()
      var i = 0
      val len = rawCoefficients.length
      while (i < len) {
        rawCoefficients(i) *= {
          if (featuresStd(i) != 0.0) 1.0 else 0.0
        }
        i += 1
      }

      (Vectors.dense(rawCoefficients).compressed, arrayBuilder.result())
    }

    /*
       The intercept in R's GLMNET is computed using closed form after the coefficients are
       converged. See the following discussion for detail.
       http://stats.stackexchange.com/questions/13617/how-is-the-intercept-computed-in-glmnet
     */
    val intercept = if ($(fitIntercept)) {
      yMedian
    } else {
      0.0
    }

    if (handlePersistence) instances.unpersist()

    val model = copyValues(new L1RegressionModel(uid, coefficients, intercept))
    // Handle possible missing or invalid prediction columns
    val (summaryModel, predictionColName) = model.findSummaryModelAndPredictionCol()

    val trainingSummary = new L1RegressionTrainingSummary(
      summaryModel.transform(dataset),
      predictionColName,
      $(labelCol),
      $(featuresCol),
      model,
      Array(0D),
      objectiveHistory)
    model.setSummary(trainingSummary)
  }

  @Since("1.4.0")
  override def copy(extra: ParamMap): L1Regression = defaultCopy(extra)
}

@Since("1.6.0")
object L1Regression extends DefaultParamsReadable[L1Regression] {

  @Since("1.6.0")
  override def load(path: String): L1Regression = super.load(path)
}

/**
  * Model produced by [[L1Regression]].
  */
@Since("1.3.0")
class L1RegressionModel private[ml](
                                     @Since("1.4.0") override val uid: String,
                                     @Since("2.0.0") val coefficients: Vector,
                                     @Since("1.3.0") val intercept: Double)
  extends RegressionModel[Vector, L1RegressionModel]
    with L1RegressionParams with MLWritable {

  private var trainingSummary: Option[L1RegressionTrainingSummary] = None

  override val numFeatures: Int = coefficients.size

  /**
    * Gets summary (e.g. residuals, mse, r-squared ) of model on training set. An exception is
    * thrown if `trainingSummary == None`.
    */
  @Since("1.5.0")
  def summary: L1RegressionTrainingSummary = trainingSummary.getOrElse {
    throw new SparkException("No training summary available for this L1RegressionModel")
  }

  private[regression] def setSummary(summary: L1RegressionTrainingSummary): this.type = {
    this.trainingSummary = Some(summary)
    this
  }

  /** Indicates whether a training summary exists for this model instance. */
  @Since("1.5.0")
  def hasSummary: Boolean = trainingSummary.isDefined

  /**
    * Evaluates the model on a test dataset.
    *
    * @param dataset Test dataset to evaluate model on.
    */
  @Since("2.0.0")
  def evaluate(dataset: Dataset[_]): L1RegressionSummary = {
    // Handle possible missing or invalid prediction columns
    val (summaryModel, predictionColName) = findSummaryModelAndPredictionCol()
    new L1RegressionSummary(summaryModel.transform(dataset), predictionColName,
      $(labelCol), $(featuresCol), summaryModel, Array(0D))
  }

  /**
    * If the prediction column is set returns the current model and prediction column,
    * otherwise generates a new column and sets it as the prediction column on a new copy
    * of the current model.
    */
  private[regression] def findSummaryModelAndPredictionCol(): (L1RegressionModel, String) = {
    $(predictionCol) match {
      case "" =>
        val predictionColName = "prediction_" + java.util.UUID.randomUUID.toString
        (copy(ParamMap.empty).setPredictionCol(predictionColName), predictionColName)
      case p => (this, p)
    }
  }


  override protected def predict(features: Vector): Double = {
    dot(features, coefficients) + intercept
  }

  @Since("1.4.0")
  override def copy(extra: ParamMap): L1RegressionModel = {
    val newModel = copyValues(new L1RegressionModel(uid, coefficients, intercept), extra)
    if (trainingSummary.isDefined) newModel.setSummary(trainingSummary.get)
    newModel.setParent(parent)
  }

  /**
    * Returns a [[org.apache.spark.ml.util.MLWriter]] instance for this ML instance.
    *
    * For [[L1RegressionModel]], this does NOT currently save the training [[summary]].
    * An option to save [[summary]] may be added in the future.
    *
    * This also does not save the [[parent]] currently.
    */
  @Since("1.6.0")
  override def write: MLWriter = new L1RegressionModel.L1RegressionModelWriter(this)
}

@Since("1.6.0")
object L1RegressionModel extends MLReadable[L1RegressionModel] {

  @Since("1.6.0")
  override def read: MLReader[L1RegressionModel] = new L1RegressionModelReader

  @Since("1.6.0")
  override def load(path: String): L1RegressionModel = super.load(path)

  /** [[MLWriter]] instance for [[L1RegressionModel]] */
  private[L1RegressionModel] class L1RegressionModelWriter(instance: L1RegressionModel)
    extends MLWriter with Logging {

    private case class Data(intercept: Double, coefficients: Vector)

    override protected def saveImpl(path: String): Unit = {
      // Save metadata and Params
      DefaultParamsWriter.saveMetadata(instance, path, sc)
      // Save model data: intercept, coefficients
      val data = Data(instance.intercept, instance.coefficients)
      val dataPath = new Path(path, "data").toString
      sparkSession.createDataFrame(Seq(data)).repartition(1).write.parquet(dataPath)
    }
  }

  private class L1RegressionModelReader extends MLReader[L1RegressionModel] {

    /** Checked against metadata when loading model */
    private val className = classOf[L1RegressionModel].getName

    override def load(path: String): L1RegressionModel = {
      val metadata = DefaultParamsReader.loadMetadata(path, sc, className)

      val dataPath = new Path(path, "data").toString
      val data = sparkSession.read.format("parquet").load(dataPath)
      val Row(intercept: Double, coefficients: Vector) =
        MLUtils.convertVectorColumnsToML(data, "coefficients")
          .select("intercept", "coefficients")
          .head()
      val model = new L1RegressionModel(metadata.uid, coefficients, intercept)

      DefaultParamsReader.getAndSetParams(model, metadata)
      model
    }
  }

}

/**
  * :: Experimental ::
  * Linear regression training results. Currently, the training summary ignores the
  * training weights except for the objective trace.
  *
  * @param predictions      predictions output by the model's `transform` method.
  * @param objectiveHistory objective function (scaled loss + regularization) at each iteration.
  */
@Since("1.5.0")
@Experimental
class L1RegressionTrainingSummary private[regression](
                                                       predictions: DataFrame,
                                                       predictionCol: String,
                                                       labelCol: String,
                                                       featuresCol: String,
                                                       model: L1RegressionModel,
                                                       diagInvAtWA: Array[Double],
                                                       val objectiveHistory: Array[Double])
  extends L1RegressionSummary(
    predictions,
    predictionCol,
    labelCol,
    featuresCol,
    model,
    diagInvAtWA) {

  /**
    * Number of training iterations until termination
    *
    * This value is only available when using the "l-bfgs" solver.
    *
    * @see [[L1Regression.solver]]
    */
  @Since("1.5.0")
  val totalIterations = objectiveHistory.length

}

/**
  * :: Experimental ::
  * Linear regression results evaluated on a dataset.
  *
  * @param predictions   predictions output by the model's `transform` method.
  * @param predictionCol Field in "predictions" which gives the predicted value of the label at
  *                      each instance.
  * @param labelCol      Field in "predictions" which gives the true label of each instance.
  * @param featuresCol   Field in "predictions" which gives the features of each instance as a vector.
  */
@Since("1.5.0")
@Experimental
class L1RegressionSummary private[regression](
                                               @transient val predictions: DataFrame,
                                               val predictionCol: String,
                                               val labelCol: String,
                                               val featuresCol: String,
                                               private val privateModel: L1RegressionModel,
                                               private val diagInvAtWA: Array[Double]) extends Serializable {

  @deprecated("The model field is deprecated and will be removed in 2.1.0.", "2.0.0")
  val model: L1RegressionModel = privateModel

  @transient private val metrics = new RegressionMetrics(
    predictions
      .select(col(predictionCol), col(labelCol).cast(DoubleType))
      .rdd
      .map { case Row(pred: Double, label: Double) => (pred, label) },
    !privateModel.getFitIntercept)

  /**
    * Returns the explained variance regression score.
    * explainedVariance = 1 - variance(y - \hat{y}) / variance(y)
    * Reference: [[http://en.wikipedia.org/wiki/Explained_variation]]
    *
    * Note: This ignores instance weights (setting all to 1.0) from [[L1Regression.weightCol]].
    * This will change in later Spark versions.
    */
  @Since("1.5.0")
  val explainedVariance: Double = metrics.explainedVariance

  /**
    * Returns the mean absolute error, which is a risk function corresponding to the
    * expected value of the absolute error loss or l1-norm loss.
    *
    * Note: This ignores instance weights (setting all to 1.0) from [[L1Regression.weightCol]].
    * This will change in later Spark versions.
    */
  @Since("1.5.0")
  val meanAbsoluteError: Double = metrics.meanAbsoluteError

  /**
    * Returns the mean squared error, which is a risk function corresponding to the
    * expected value of the squared error loss or quadratic loss.
    *
    * Note: This ignores instance weights (setting all to 1.0) from [[L1Regression.weightCol]].
    * This will change in later Spark versions.
    */
  @Since("1.5.0")
  val meanSquaredError: Double = metrics.meanSquaredError

  /**
    * Returns the root mean squared error, which is defined as the square root of
    * the mean squared error.
    *
    * Note: This ignores instance weights (setting all to 1.0) from [[L1Regression.weightCol]].
    * This will change in later Spark versions.
    */
  @Since("1.5.0")
  val rootMeanSquaredError: Double = metrics.rootMeanSquaredError

  /**
    * Returns R^2^, the coefficient of determination.
    * Reference: [[http://en.wikipedia.org/wiki/Coefficient_of_determination]]
    *
    * Note: This ignores instance weights (setting all to 1.0) from [[L1Regression.weightCol]].
    * This will change in later Spark versions.
    */
  @Since("1.5.0")
  val r2: Double = metrics.r2

  /** Residuals (label - predicted value) */
  @Since("1.5.0")
  @transient lazy val residuals: DataFrame = {
    val t = udf { (pred: Double, label: Double) => label - pred }
    predictions.select(t(col(predictionCol), col(labelCol)).as("residuals"))
  }

  /** Number of instances in DataFrame predictions */
  lazy val numInstances: Long = predictions.count()

  /** Degrees of freedom */
  private val degreesOfFreedom: Long = if (privateModel.getFitIntercept) {
    numInstances - privateModel.coefficients.size - 1
  } else {
    numInstances - privateModel.coefficients.size
  }

  /**
    * The weighted residuals, the usual residuals rescaled by
    * the square root of the instance weights.
    */
  lazy val devianceResiduals: Array[Double] = {
    val weighted =
      if (!privateModel.isDefined(privateModel.weightCol) || privateModel.getWeightCol.isEmpty) {
        lit(1.0)
      } else {
        sqrt(col(privateModel.getWeightCol))
      }
    val dr = predictions
      .select(col(privateModel.getLabelCol).minus(col(privateModel.getPredictionCol))
        .multiply(weighted).as("weightedResiduals"))
      .select(min(col("weightedResiduals")).as("min"), max(col("weightedResiduals")).as("max"))
      .first()
    Array(dr.getDouble(0), dr.getDouble(1))
  }

  /**
    * Standard error of estimated coefficients and intercept.
    * This value is only available when using the "normal" solver.
    *
    * If [[L1Regression.fitIntercept]] is set to true,
    * then the last element returned corresponds to the intercept.
    *
    * @see [[L1Regression.solver]]
    */
  lazy val coefficientStandardErrors: Array[Double] = {
    if (diagInvAtWA.length == 1 && diagInvAtWA(0) == 0) {
      throw new UnsupportedOperationException(
        "No Std. Error of coefficients available for this L1RegressionModel")
    } else {
      val rss =
        if (!privateModel.isDefined(privateModel.weightCol) || privateModel.getWeightCol.isEmpty) {
          meanSquaredError * numInstances
        } else {
          val t = udf { (pred: Double, label: Double, weight: Double) =>
            math.pow(label - pred, 2.0) * weight
          }
          predictions.select(t(col(privateModel.getPredictionCol), col(privateModel.getLabelCol),
            col(privateModel.getWeightCol)).as("wse")).agg(sum(col("wse"))).first().getDouble(0)
        }
      val sigma2 = rss / degreesOfFreedom
      diagInvAtWA.map(_ * sigma2).map(math.sqrt)
    }
  }

  /**
    * T-statistic of estimated coefficients and intercept.
    * This value is only available when using the "normal" solver.
    *
    * If [[L1Regression.fitIntercept]] is set to true,
    * then the last element returned corresponds to the intercept.
    *
    * @see [[L1Regression.solver]]
    */
  lazy val tValues: Array[Double] = {
    if (diagInvAtWA.length == 1 && diagInvAtWA(0) == 0) {
      throw new UnsupportedOperationException(
        "No t-statistic available for this L1RegressionModel")
    } else {
      val estimate = if (privateModel.getFitIntercept) {
        Array.concat(privateModel.coefficients.toArray, Array(privateModel.intercept))
      } else {
        privateModel.coefficients.toArray
      }
      estimate.zip(coefficientStandardErrors).map { x => x._1 / x._2 }
    }
  }

  /**
    * Two-sided p-value of estimated coefficients and intercept.
    * This value is only available when using the "normal" solver.
    *
    * If [[L1Regression.fitIntercept]] is set to true,
    * then the last element returned corresponds to the intercept.
    *
    * @see [[L1Regression.solver]]
    */
  lazy val pValues: Array[Double] = {
    if (diagInvAtWA.length == 1 && diagInvAtWA(0) == 0) {
      throw new UnsupportedOperationException(
        "No p-value available for this L1RegressionModel")
    } else {
      tValues.map { x => 2.0 * (1.0 - StudentsT(degreesOfFreedom.toDouble).cdf(math.abs(x))) }
    }
  }

}

private class L1Aggregator(
                            coefficients: Vector,
                            labelStd: Double,
                            labelMedian: Double) extends Serializable {

  private var totalCnt: Long = 0L
  private var weightSum: Double = 0.0
  private var lossSum = 0.0

  private val (effectiveCoefficientsArray: Array[Double], offset: Double, dim: Int) = {
    val coefficientsArray = coefficients.toArray.clone()
    val offset = labelMedian
    (coefficientsArray, offset, coefficientsArray.length)
  }

  private val effectiveCoefficientsVector = Vectors.dense(effectiveCoefficientsArray)

  private val gradientSumArray = Array.ofDim[Double](dim)

  private def logcosh_diff1(x: Double) = {
    (math.log(math.cosh(x)), math.tanh(x))
  }

  private def fair_diff1(x: Double) = {
    (math.abs(x) - math.log(math.abs(x) + 1), x / (math.abs(x) + 1))
  }

  private def abs_smooth_diff1(x: Double) = {
    val a= math.sqrt(x*x+0.0001)
    (a , x /a)
  }


  /**
    * Add a new training instance to this L1LeastSquaresAggregator, and update the loss and gradient
    * of the objective function.
    *
    * @param instance The instance of data point to be added.
    * @return This L1LeastSquaresAggregator object.
    */


  def add(instance: Instance): this.type = {
    instance match {
      case Instance(label, weight, features) =>
        require(dim == features.size, s"Dimensions mismatch when adding new sample." +
          s" Expecting $dim but got ${features.size}.")
        require(weight >= 0.0, s"instance weight, $weight has to be >= 0.0")

        if (weight == 0.0) return this

        val diff = dot(features, effectiveCoefficientsVector) + offset - label

        if (diff != 0) {
          val localGradientSumArray = gradientSumArray
          //val (v, grad) = abs_smooth_diff1(diff)
          val (v, grad) = logcosh_diff1(diff)
          features.foreachActive { (index, value) =>
            if (value != 0.0) {
              localGradientSumArray(index) += weight * grad * value
            }
          }
          lossSum += weight * v // math.abs(diff)
        }

        totalCnt += 1
        weightSum += weight
        this
    }
  }

  /**
    * Merge another L1LeastSquaresAggregator, and update the loss and gradient
    * of the objective function.
    * (Note that it's in place merging; as a result, `this` object will be modified.)
    *
    * @param other The other L1LeastSquaresAggregator to be merged.
    * @return This L1LeastSquaresAggregator object.
    */
  def merge(other: L1Aggregator): this.type = {
    require(dim == other.dim, s"Dimensions mismatch when merging with another " +
      s"L1LeastSquaresAggregator. Expecting $dim but got ${other.dim}.")

    if (other.weightSum != 0) {
      totalCnt += other.totalCnt
      weightSum += other.weightSum
      lossSum += other.lossSum

      var i = 0
      val localThisGradientSumArray = this.gradientSumArray
      val localOtherGradientSumArray = other.gradientSumArray
      while (i < dim) {
        localThisGradientSumArray(i) += localOtherGradientSumArray(i)
        i += 1
      }
    }
    this
  }

  def count: Long = totalCnt

  def loss: Double = {
    require(weightSum > 0.0, s"The effective number of instances should be " +
      s"greater than 0.0, but $weightSum.")
    lossSum / weightSum
  }

  def gradient: Vector = {
    require(weightSum > 0.0, s"The effective number of instances should be " +
      s"greater than 0.0, but $weightSum.")
    val result = Vectors.dense(gradientSumArray.clone())
    scal(1.0 / weightSum, result)
    result
  }
}

/**
  * L1LeastSquaresCostFun implements Breeze's DiffFunction[T] for Least Squares cost.
  * It returns the loss and gradient with L2 regularization at a particular point (coefficients).
  * It's used in Breeze's convex optimization routines.
  */
private class L1CostFun(
                         instances: RDD[Instance],
                         labelStd: Double,
                         labelMedian: Double,
                         effectiveL2regParam: Double) extends DiffFunction[BDV[Double]] {

  override def calculate(coefficients: BDV[Double]): (Double, BDV[Double]) = {
    val coeffs = Vectors.fromBreeze(coefficients)

    val l1Aggregator = {
      val seqOp = (c: L1Aggregator, instance: Instance) => c.add(instance)
      val combOp = (c1: L1Aggregator, c2: L1Aggregator) => c1.merge(c2)

      instances.treeAggregate(
        new L1Aggregator(coeffs, labelStd, labelMedian))(seqOp, combOp)
    }

    val totalGradientArray = l1Aggregator.gradient.toArray

    val regVal = if (effectiveL2regParam == 0.0) {
      0.0
    } else {
      var sum = 0.0
      coeffs.foreachActive { (index, value) =>
        sum += {
          totalGradientArray(index) += effectiveL2regParam * value
          value * value
        }
      }
      0.5 * effectiveL2regParam * sum
    }

    (l1Aggregator.loss + regVal, new BDV(totalGradientArray))
  }
}

