TRAIN=../input/train2016_cat.csv
TARGET=target/scala-2.11/z2017-assembly-1.0.jar

MAIN=zillow.SparkGenLibsvmData2
WAIT=1
ENABLE_LOG=false

FEATS=lass_feats_30_0.003_1.0-lass_feats_20_0.003_0.8.txt
#all_field_count.txt.reduced.sorted2
 
nohup $HOME/spark/bin/spark-submit --master spark://genomics-ecs1:7077 --deploy-mode client --driver-memory 55G --driver-cores 5 --executor-memory 20G  --executor-cores 2 --conf spark.default.parallelism=8100 --conf spark.driver.maxResultSize=5g --conf spark.network.timeout=360000 --conf spark.eventLog.enabled=$ENABLE_LOG --class $MAIN $TARGET \
$TRAIN $FEATS  &
