TRAIN=../input/train2016_cat.csv
TARGET=target/scala-2.11/z2017-assembly-1.0.jar

MAIN=zillow.SparkFeatureSort2
WAIT=1
ENABLE_LOG=false
 
nohup $HOME/spark/bin/spark-submit --master spark://genomics-ecs1:7077 --deploy-mode client --driver-memory 55G --driver-cores 5 --executor-memory 20G  --executor-cores 2 --conf spark.default.parallelism=8100 --conf spark.driver.maxResultSize=5g --conf spark.network.timeout=360000 --conf spark.eventLog.enabled=$ENABLE_LOG --class $MAIN $TARGET \
$TRAIN all_field_count.txt.reduced &
