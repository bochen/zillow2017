TRAIN=../input/train2016_cat.csv
N=50
MINCOUNT=50
MAIN=zillow.SparkFeatureCount2
WAIT=1
ENABLE_LOG=false
TARGET=target/scala-2.11/z2017-assembly-1.0.jar
 
nohup $HOME/spark/bin/spark-submit --master spark://genomics-ecs1:7077 --deploy-mode client --driver-memory 55G --driver-cores 5 --executor-memory 20G  --executor-cores 2 --conf spark.default.parallelism=3000 --conf spark.driver.maxResultSize=8g --conf spark.network.timeout=360000 --conf spark.eventLog.enabled=$ENABLE_LOG --class $MAIN $TARGET \
--wait $WAIT --wait_input  -i $TRAIN -n $N --min_count $MINCOUNT -k 200000 -s 0 &
