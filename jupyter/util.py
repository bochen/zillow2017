import numpy as np
import json
import os
import ntpath
import glob
    
def readtext(filename):
    lines=[]
    with open(filename,'r') as f:
        for line in f:
            lines.append(line.strip())
    return np.array(lines)
def writetext(filename,lines):
    with open(filename,'w') as f:
        for line in lines:
            f.write(line+'\n')

def writeObj2Txt(filename,objs):
    lines=[json.dumps(u) for u in objs]
    writetext(filename,lines)
    
def readObj(filename):
    return [json.loads(s) for s in readtext(filename)]


########### transform data 
def f_combine(s,m,t=None):
    assert(t is not None)
    def f(v):
        if v in m:
            return m[v]
        else:
            return v
    return s.fillna(-100).astype(t).apply(f)


f_identity=lambda  s: s

#categoroes
trans_airconditioningtypeid = lambda v: f_combine(v,{3:1,9:5,12:11}, t=np.int32)
trans_architecturalstyletypeid = lambda v: f_combine(v,{2:7,3:7,5:7,8:7,10:7,21:7,27:7}, t=np.int32)
def trans_bathroomcnt(s):    s=np.round(s,0);     s[s>9]=9;     return s
def trans_bedroomcnt(s):     s=s.copy();  s[s>9]=9;     return s
trans_buildingqualitytypeid = lambda v: f_combine(v,{11:12,8:10,5:7,6:7,2:4,3:4,9:10}, t=np.int32)
def trans_calculatedbathnbr(s):
    s=s.copy()
    s[s>8]=8
    s[s==6.5]=6
    s[s==7.5]=7
    return s
def trans_fireplacecnt(s): s=s.copy(); s[s>3]=3;return s
def trans_fullbathcnt(s): s=s.copy(); s[s>9]=9;return s
def trans_garagecarcnt(s): s=s.copy(); s[s>5]=5;return s
def trans_heatingorsystemtypeid(s): s=s.copy(); s[(s>=10) & (s<=21)]=18; s[s==1]=18; return s
def trans_propertycountylandusecode(s):
    codes=set(['1128', '1129', '0400', '1111', '010V', '1', '1110', '01DC', '012C', '0200', '010E', '010D', '010C', '0300', '01HC', '0104', '0101', '0100', '122', '96', '38', '34'])
    s=s.copy()
    s[~s.isin(codes)]="XXXX"
    return s
def trans_propertylandusetypeid(s):
    codes=set([261.0, 265.0, 266.0, 269.0, 246.0, 247.0, 248.0])
    s=s.copy()
    s[~s.isin(codes)]=999
    return s.astype(np.int32)

def trans_propertyzoningdesc(s): 
    codes=set(['A', 'C', 'B', 'E', 'D', 'G', 'I', 'H', 'M', 'L', 'n', 'P', 'S', 'R', 'T', 'W', 'N'])
    s=s.apply(lambda u: str(u)[0]);  
    s[~s.isin(codes)]="X";
    return s
def trans_regionidcity(s): 
    codes=set([46080.0, 10241.0, 12292.0, 46098.0, 10774.0, 45602.0, 54311.0, 20008.0, 33836.0, 33837.0, 33840.0, 50749.0, 44116.0, 47198.0, 52835.0, 52842.0, 24174.0, 9840.0, 32380.0, 25218.0, 396054.0, 38032.0, 40081.0, 47762.0, 51861.0, 37015.0, 32923.0, 12447.0, 42150.0, 25459.0, 24245.0, 30908.0, 17597.0, 8384.0, 15554.0, 41673.0, 14542.0, 46298.0, 37086.0, 12520.0, 19177.0, 51239.0, 24812.0, 34543.0, 17150.0, 24832.0, 17686.0, 14111.0, 44833.0, 40227.0, 54053.0, 48424.0, 14634.0, 22827.0, 4406.0, 37688.0, 45888.0, 53571.0, 33612.0, 26964.0, 45398.0, 5465.0, 13150.0, 25953.0, 27491.0, 27110.0, 11626.0, 10608.0, 25458.0, 26483.0, 25974.0, 16764.0, 13693.0, 10389.0, 24384.0, 53636.0, 6021.0, 39308.0, 45457.0, 5534.0, 51617.0, 26531.0, 21412.0, 52650.0, 47019.0, 18874.0, 54722.0, 54212.0, 47568.0, 118225.0, 17882.0, 34780.0, 10723.0, 33252.0, 12773.0, 34278.0, 30187.0, 10734.0, 50677.0])
    s=s.copy()  
    s[~s.isin(codes)]=-99999;
    return s.astype(np.int32)
def trans_regionidneighborhood(s): 
    codes=set(['318', '276', '210', '762', '763', '761', '331', '764', '115', '114', '272', '116', '274', '275', '113', '279', '323', '118', '198', '414', '695', 'nan', '320', '130', '543', '405', '403', '402', '342', '131', '467', '307', '261', '485', '268', '482', '270', '787', '117', '519', '378', '416', '273', '411', '478', '479', '377'])
    s=s.apply(lambda u: str(u)[0:3]);  
    s[~s.isin(codes)]="XXX";
    return s
def trans_regionidzip1(s): 
    codes=set(['9731', '9616', '9703', '9732', '9612', '9613', '9610', '9611', '9647', 'nan', '9615', '9733', '9618', '9619', '9638', '9700', '9650', '9651', '9599', '9598', '9644', '9711', '9710', '9634', '9635', '9636', '9637', '9633', '9653', '9641', '9632', '9621', '9640', '9643', '9608', '9707', '9705', '9601', '9600', '9603', '9602', '9605', '9604', '9701', '9609', '9617', '9620', '9645', '9708', '9709', '9629', '9628', '9649', '9648', '9623', '9622', '9702', '9646', '9704', '9626', '9706', '9624', '9652', '9639', '9627', '9642', '9698', '9699', '9696', '9697', '9694', '9695', '9693'])
    s=s.apply(lambda u: str(u)[0:4]);  
    s[~s.isin(codes)]="XXX";
    return s
def trans_regionidzip(s):
    return [trans_regionidzip1(s),trans_regionidzip2(s)]
def trans_regionidzip2(s): 
    codes=set(['964', '965', '959', '960', '961', '962', '970', '963', 'nan', '969', '971', '973'])
    s=s.apply(lambda u: str(u)[0:3]);  
    s[~s.isin(codes)]="XXX";
    return s
def trans_roomcnt(s):
    s=s.fillna(0)
    s[s<=2]=0
    s[s>11]=11
    return s
def trans_threequarterbathnbr(s):
    s=s.copy()
    s[s>2]=2
    return s
def trans_typeconstructiontypeid(s):
    s=s.copy()
    s[s!=6]=-100
    return s
def trans_unitcnt(s):
    s=s.copy()
    s[s>4]=4
    return s
def trans_yearbuilt(s): 
    codes=set(['201', '200', '199', '198', '195', '194', '197', '196', 'nan', '190', '193', '192', '191'])
    s=s.apply(lambda u: str(u)[0:3]);  
    s[~s.isin(codes)]="XXX";
    return s
def trans_numberofstories(s): s=s.copy(); s[s>3]=3; return s
def trans_taxdelinquencyyear(s): s=s.copy(); s[s>-1]=1; return s
def trans_censustractandblock(s): 
    codes=set(['603791', '603790', '603792', 'nan', '605901', '605906', '605907', '605904', '605905', '605902', '605903', '605900', '611100', '605908', '605909', '603743', '603740', '603746', '603765', '603748', '603767', '603760', '603762', '603729', '603720', '603721', '603722', '603723', '603724', '603726', '603727', '603731', '603780', '605911', '603750', '603753', '603755', '603754', '603757', '603730', '603770', '603719', '603718', '603714', '603711', '603710', '603713', '603712'])
    s=s.apply(lambda u: str(u)[0:6]);  
    s[~s.isin(codes)]="XXX";
    return s

trans_parcelid = f_identity
trans_decktypeid=f_identity
trans_fips=f_identity
trans_hashottuborspa=f_identity
trans_poolcnt=f_identity
trans_pooltypeid10=f_identity
trans_pooltypeid2=f_identity
trans_pooltypeid7=f_identity
trans_regionidcounty=f_identity
trans_storytypeid=f_identity
trans_fireplaceflag=f_identity
trans_taxdelinquencyflag=f_identity

def non_trans(v): return None
trans_buildingclasstypeid =non_trans
trans_latitude =non_trans
trans_longitude =non_trans
trans_rawcensustractandblock =non_trans
trans_assessmentyear =non_trans

#numerical
sqrt_trans = lambda v:  np.round(np.sqrt(v),0)
trans_basementsqft =   sqrt_trans
trans_finishedfloor1squarefeet =   sqrt_trans
trans_calculatedfinishedsquarefeet=   sqrt_trans
trans_finishedsquarefeet12=   sqrt_trans
trans_finishedsquarefeet15=   sqrt_trans
trans_finishedsquarefeet50=   sqrt_trans
trans_finishedsquarefeet6=   sqrt_trans
trans_lotsizesquarefeet =   sqrt_trans
trans_poolsizesum=   sqrt_trans
trans_yardbuildingsqft17=   sqrt_trans
trans_yardbuildingsqft26=   sqrt_trans
trans_garagetotalsqft=   sqrt_trans

trans_structuretaxvaluedollarcnt=f_identity
trans_taxvaluedollarcnt=f_identity
trans_landtaxvaluedollarcnt=f_identity
trans_taxamount=f_identity
trans_finishedsquarefeet13=f_identity


def transform(col, df):
    funname="trans_"+col
    if funname in  globals():
        fun=globals()[funname]
        ret = fun(df[col])
        if type(ret) is list:
            ret=[u for u in ret if u is not None]
            if len(ret)>0:
                return {col+str(i):ret[i].fillna(-100) for i in range(len(ret))}
            else:
                return None
        else:
            if ret is not None: 
                ret=ret.fillna(-100)
                return {col:ret}
            else:
                return None
    else:
        print "Cannot find function", funname
        

########### for transformed data
COL2_NUM=set(["basementsqft",'finishedfloor1squarefeet','calculatedfinishedsquarefeet',
              'finishedsquarefeet12',
              'finishedsquarefeet13',
              'finishedsquarefeet15',
              'finishedsquarefeet50',
              'finishedsquarefeet6',
              'garagetotalsqft',
              'lotsizesquarefeet',
              'landtaxvaluedollarcnt',
              'structuretaxvaluedollarcnt',
              'poolsizesum',
              'taxamount',
              'taxvaluedollarcnt',
              'yardbuildingsqft17',
              'yardbuildingsqft26',
              
             ])
        